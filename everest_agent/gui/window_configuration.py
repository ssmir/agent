# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import re
from . import qt
from . import window_list_edit
from six.moves import filter
from six.moves import map

class WindowConfiguration(qt.QtGui.QDialog):
  """
  Configuration window.
  """
  _MIN_WIDTH = 600
  _MIN_HEIGHT = 480

  _WHITELIST_ANY_ID = 0
  _WHITELIST_CUSTOM_ID = 1

  @staticmethod
  def show_modal(parent, model):
    """
    Show new configuration window (synchronously)
    """
    assert not model.backend.is_running
    dialog = WindowConfiguration(parent, model)
    dialog.setAttribute(qt.QtCore.Qt.WA_DeleteOnClose)
    return dialog.exec_()

  def __init__(self, parent, model):
    """
    Init new configuration window.
    """
    qt.QtGui.QDialog.__init__(self, parent)
    self.__model = model
    # store agent attributes separately, as they are edited in a modal window
    self.__agent_attributes = self.__model.config.agent_attributes
    # init window properties
    self.setWindowTitle(self.tr("Configuration"))
    self.setMinimumSize(self._MIN_WIDTH, self._MIN_HEIGHT)
    # init layout
    layout = qt.QtGui.QVBoxLayout()
    layout.addLayout(self.__init_form())
    layout.addStretch(1)
    layout.addLayout(self.__init_buttons())
    self.setLayout(layout)

  def __init_form(self):
    """
    Get initialized options layout.
    """
    layout = qt.QtGui.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)

    def _instruction_label(instruction):
      instruction_label = qt.QtGui.QLabel(instruction)
      instruction_label.setProperty(qt.INSTRUCTION_PROPERTY, qt.INSTRUCTION_TRUE)
      return instruction_label

    general_group = qt.QtGui.QGroupBox(self.tr("General"))
    self.__passive_mode_group = qt.QtGui.QGroupBox(self.tr("Passive mode"))
    self.__active_mode_group = qt.QtGui.QGroupBox(self.tr("Active mode"))

    self.__passive_mode_group.setCheckable(True)
    self.__active_mode_group.setCheckable(True)

    self.__passive_mode_group.setChecked(self.__model.config.passive_mode_enabled)
    self.__active_mode_group.setChecked(self.__model.config.active_mode_enabled)

    general_layout = qt.QtGui.QGridLayout()
    passive_mode_layout = qt.QtGui.QGridLayout()
    active_mode_layout = qt.QtGui.QGridLayout()

    for grid_layout in [general_layout, passive_mode_layout, active_mode_layout]:
      grid_layout.setColumnMinimumWidth(0, 140)

    general_layout.setColumnStretch(2, 1)
    passive_mode_layout.setColumnStretch(1, 1)
    active_mode_layout.setColumnStretch(1, 1)

    self.__input_task_port = qt.QtGui.QSpinBox()
    self.__input_task_port.setRange(1, 2**16 - 1)
    self.__input_task_port.setValue(self.__model.config.task_port)

    self.__input_control_port = qt.QtGui.QSpinBox()
    self.__input_control_port.setRange(1, 2**16 - 1)
    self.__input_control_port.setValue(self.__model.config.control_port)

    self.__input_use_whitelist = qt.QtGui.QButtonGroup()
    whitelist_any_button = qt.QtGui.QRadioButton(self.tr("Any"))
    whitelist_list_button = qt.QtGui.QRadioButton(self.tr("Whitelist:"))
    self.__input_use_whitelist.addButton(whitelist_any_button, self._WHITELIST_ANY_ID)
    self.__input_use_whitelist.addButton(whitelist_list_button, self._WHITELIST_CUSTOM_ID)
    whitelist_any_button.setChecked(self.__model.config.whitelist is None)
    whitelist_list_button.setChecked(self.__model.config.whitelist is not None)

    self.__input_whitelist = qt.QtGui.QLineEdit()
    self.__input_whitelist_instruction = _instruction_label(self.tr("Example: ls .*:python test_.*.py"))
    if self.__model.config.whitelist is not None:
      self.__input_whitelist.setText(":".join([s.replace(":", "::") for s in self.__model.config.whitelist]))
    self.__input_whitelist.setEnabled(self.__model.config.whitelist is not None)
    self.__input_whitelist_instruction.setEnabled(self.__model.config.whitelist is not None)

    whitelist_list_button.toggled.connect(self.__input_whitelist.setEnabled)
    whitelist_list_button.toggled.connect(self.__input_whitelist_instruction.setEnabled)

    self.__input_agent_autostart = qt.QtGui.QCheckBox()
    self.__input_agent_autostart.setCheckState(qt.QtCore.Qt.Checked if self.__model.config.agent_autostart else qt.QtCore.Qt.Unchecked)

    self.__status_agent_attributes = qt.QtGui.QLabel()
    self.__status_agent_attributes.setText(", ".join(sorted(self.__agent_attributes.keys())))
    self.__input_agent_attributes = qt.QtGui.QPushButton(self.tr("Edit..."))
    self.__input_agent_attributes.clicked.connect(self.__edit_attributes)

    self.__input_port = qt.QtGui.QSpinBox()
    self.__input_port.setRange(1, 2**16 - 1)
    self.__input_port.setValue(self.__model.config.agent_port)

    self.__input_client_tokens = qt.QtGui.QLineEdit()
    self.__input_client_tokens.setText(":".join(self.__model.config.client_tokens))
    self.__input_client_tokens.setValidator(qt.QtGui.QRegExpValidator(qt.QtCore.QRegExp(r"[A-Za-z0-9]+((:\s*)[A-Za-z0-9]+)*[A-Za-z0-9]*")))

    self.__input_agent_token = qt.QtGui.QLineEdit()
    self.__input_agent_token.setText(self.__model.config.agent_token)
    self.__input_agent_token.setValidator(qt.QtGui.QRegExpValidator(qt.QtCore.QRegExp(r"[A-Za-z\d]*")))

    # fix checkbox stretching - without additional stretcher 'pressure'
    # it spans across whole form row, which is noticeable on hover highlight
    autostart_layout = qt.QtGui.QHBoxLayout()
    autostart_layout.setContentsMargins(0, 0, 0, 0)
    autostart_layout.addWidget(self.__input_agent_autostart)
    autostart_layout.addStretch()

    attributes_layout = qt.QtGui.QHBoxLayout()
    attributes_layout.setContentsMargins(0, 0, 0, 0)
    attributes_layout.addWidget(self.__status_agent_attributes, 1)
    attributes_layout.addWidget(self.__input_agent_attributes)

    general_layout.addWidget(qt.QtGui.QLabel(self.tr("Tasks port:")), 0, 0)
    general_layout.addWidget(self.__input_task_port, 0, 1, 1, 2)
    general_layout.addWidget(qt.QtGui.QLabel(self.tr("Control port:")), 1, 0)
    general_layout.addWidget(self.__input_control_port, 1, 1, 1, 2)
    general_layout.addWidget(qt.QtGui.QLabel(self.tr("Allowed commands:")), 2, 0)
    general_layout.addWidget(whitelist_any_button, 2, 1)
    general_layout.addWidget(whitelist_list_button, 3, 1)
    general_layout.addWidget(self.__input_whitelist, 3, 2)
    general_layout.addWidget(self.__input_whitelist_instruction, 4, 2)
    general_layout.addWidget(qt.QtGui.QLabel(self.tr("Autostart agent:")), 5, 0)
    general_layout.addLayout(autostart_layout, 5, 1, 1, 2)
    general_layout.addWidget(qt.QtGui.QLabel(self.tr("Custom attributes:")), 6, 0)
    general_layout.addLayout(attributes_layout, 6, 1, 1, 2)

    passive_mode_layout.addWidget(qt.QtGui.QLabel(self.tr("Port:")), 0, 0)
    passive_mode_layout.addWidget(self.__input_port, 0, 1)
    passive_mode_layout.addWidget(qt.QtGui.QLabel(self.tr("Client tokens:")), 1, 0)
    passive_mode_layout.addWidget(self.__input_client_tokens, 1, 1)
    passive_mode_layout.addWidget(_instruction_label(self.tr("Example: TOKEN_A:TOKEN_B")), 2, 1)

    active_mode_layout.addWidget(qt.QtGui.QLabel(self.tr("Agent token:")), 0, 0)
    active_mode_layout.addWidget(self.__input_agent_token, 0, 1)

    general_group.setLayout(general_layout)
    self.__passive_mode_group.setLayout(passive_mode_layout)
    self.__active_mode_group.setLayout(active_mode_layout)

    layout.addWidget(general_group)
    layout.addWidget(self.__passive_mode_group)
    layout.addWidget(self.__active_mode_group)
    return layout

  def __init_buttons(self):
    """
    Get ok/cancel buttons layout.
    """
    layout = qt.QtGui.QHBoxLayout()

    button_ok = qt.QtGui.QPushButton(self.tr("OK"))
    button_ok.setDefault(True)
    button_cancel = qt.QtGui.QPushButton(self.tr("Cancel"))

    button_ok.clicked.connect(self.accept)
    button_cancel.clicked.connect(self.reject)

    layout.addStretch(1)
    layout.addWidget(button_ok)
    layout.addWidget(button_cancel)

    return layout

  def __edit_attributes(self):
    result = window_list_edit.WindowListEdit.show_modal(self, list(map(list, sorted(self.__agent_attributes.items()))), {
      "window_title": self.tr("Custom attributes"),
      "columns": [
        {
          "name": "Name",
          "validate": {
            "type": "unique",
            "message": self.tr("Attribute names must be unique")
          }
        },
        {
          "name": "Value"
        }
      ]
    })
    self.__agent_attributes = dict(result)
    self.__status_agent_attributes.setText(", ".join(sorted(self.__agent_attributes.keys())))

  def accept(self):
    """
    Ok/enter handler.
    """
    if not (self.__active_mode_group.isChecked() or self.__passive_mode_group.isChecked()):
      qt.QtGui.QMessageBox.warning(self, self.tr("Invalid configuration"), self.tr("At least one mode (active or passive) must be enabled."))
      return

    whitelist = None
    if self.__input_use_whitelist.checkedId() == self._WHITELIST_CUSTOM_ID:
      whitelist = [s.replace("::", ":") for s in re.split("(?<!:):(?!:)", self.__input_whitelist.text())]

    self.__model.config.active_mode_enabled = self.__active_mode_group.isChecked()
    self.__model.config.passive_mode_enabled = self.__passive_mode_group.isChecked()
    self.__model.config.agent_port = self.__input_port.value()
    self.__model.config.whitelist = whitelist
    self.__model.config.client_tokens =  list(filter(bool, [tok.strip() for tok in self.__input_client_tokens.text().split(":")]))
    self.__model.config.agent_token = self.__input_agent_token.text()
    self.__model.config.task_port = self.__input_task_port.value()
    self.__model.config.control_port = self.__input_control_port.value()
    self.__model.config.agent_autostart = self.__input_agent_autostart.checkState() == qt.QtCore.Qt.Checked
    self.__model.config.agent_attributes = self.__agent_attributes

    self.__model.config.persist()

    super(WindowConfiguration, self).accept()

