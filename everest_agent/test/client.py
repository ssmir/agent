# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import print_function

from __future__ import absolute_import
import os
import json
import argparse
import traceback
import logging

from tornado import gen
from tornado import ioloop
from tornado import httpclient
from tornado.websocket import websocket_connect

from everest_agent import utils
from six.moves import zip

MAX_BODY_SIZE = 200000000

class Client(object):
    def __init__(self, args):
        self.args = args

    def sendMessage(self, msgType, *args):
        self.conn.write_message(json.dumps([msgType] + list(args)))

    @gen.coroutine
    def loop(self):
        client = httpclient.AsyncHTTPClient(max_buffer_size=MAX_BODY_SIZE)

        taskSpec = {
            "command" : self.args.command,
            "name" : self.args.name,
            "description" : self.args.description,
            "inputData" : [],
            "outputData" : [{
                "paths": ['stdout']
            }]}

        req = httpclient.HTTPRequest(
            'ws://%s/agent/ws' % self.args.host, validate_cert=False, headers = {
                'Authorization' : 'EverestAgentClientToken %s' % self.args.token,
                'Sec-WebSocket-Protocol' : 'v1.agent.everest'})
        self.conn = yield websocket_connect(req)
        assert(self.conn.headers['Sec-WebSocket-Protocol'] == 'v1.agent.everest')
        self.sendMessage('HELLO', {})
        msg = yield self.conn.read_message()
        msg = json.loads(msg)
        assert(msg[0] == 'WELCOME')
        print(msg)

        futures = []
        for fname in self.args.files:
            futures.append(utils.uploadFile(
                client, 'http://%s/agent/upload' % self.args.host, fname,
                method = 'POST', headers = {
                    'Authorization' : 'EverestAgentClientToken %s' % self.args.token}))
        responses = yield futures
        for fname, response in zip(self.args.files, responses):
            base = os.path.basename(fname)
            datum = {
                'uri' : response.headers['Location'],
                'path' : base
            }
            if fname.endswith('tar.gz'):
                datum['unpack'] = base.split('.')[0]
            taskSpec['inputData'].append(datum)

        self.taskId = str(os.getpid())
        self.sendMessage('TASK_SUBMIT', self.taskId, taskSpec, {})
        
        while True:
            msg = yield self.conn.read_message()
            if msg is None:
                print('Connection closed')
                ioloop.IOLoop.current().stop()
                return
            msg = json.loads(msg)
            print(msg)
            if msg[0] == 'TASK_STATE' and msg[1] != self.taskId:
                print("Ignoring hanging task's info")
                continue
            if msg[0] == 'TASK_STATE' and msg[2] in ['DONE', 'FAILED', 'CANCELED']:
                self.sendMessage('TASK_DELETE', self.taskId)
                ioloop.IOLoop.current().stop()
                return
            elif msg[0] == 'TASK_STATE' and msg[2] == 'STAGED_OUT':
                for out in msg[3]['outputData']:
                    with open(out['path'], 'wb') as f:
                        yield client.fetch(
                            'http://%s%s' % (self.args.host, out['uri']),
                            streaming_callback=f.write, headers = {
                                'Authorization' : 'EverestAgentClientToken %s' % self.args.token})

def makeParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('host', action = 'store',
                        help = "Agent's URI host part, e.g. localhost:8899")
    parser.add_argument('token', action = 'store',
                        help = "Client's authentication token")
    parser.add_argument('command', action = 'store',
                        help = "Tasks's command")
    parser.add_argument('files', action = 'store',
                        nargs = argparse.REMAINDER,
                        help = 'Files to upload')
    parser.add_argument('-n', '--name', default='',
                        help = "Task name")
    parser.add_argument('-d', '--description', default='',
                        help = "Task description")
    return parser

def handleException(callback):
    traceback.print_exc()
    ioloop.IOLoop.current().stop()

def main():
    logging.getLogger('tornado.access').setLevel(logging.DEBUG)
    logging.getLogger('tornado.application').setLevel(logging.DEBUG)
    logging.getLogger('tornado.general').setLevel(logging.DEBUG)
    parser = makeParser()
    args = parser.parse_args()
    client = Client(args)
    ioloop.IOLoop.current().add_future(gen.moment, lambda f: client.loop())
    ioloop.IOLoop.current().handle_callback_exception = handleException
    ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
