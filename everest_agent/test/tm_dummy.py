from __future__ import print_function

import os
import sys
import socket
import struct
import time

port = int(os.environ['EVEREST_AGENT_PORT'])
taskId = os.environ['EVEREST_AGENT_TASK_ID']

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', port))

print('%f Sending %d bytes' % (time.time(), len(taskId)+1))
s.sendall(struct.pack('>Ib', len(taskId)+1, 0))
s.sendall(taskId.encode())

print('%f Sending %d bytes' % (time.time(), len(sys.argv[1])))
s.sendall(struct.pack('>I', len(sys.argv[1])))
s.sendall(sys.argv[1].encode())

size, = struct.unpack('>I', s.recv(4))
data = s.recv(size).decode()
s.close()
print(time.time(), 'Received', data)

if data != sys.argv[2]:
    sys.exit(1)
sys.exit(0)
