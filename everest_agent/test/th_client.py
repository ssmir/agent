#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
from __future__ import print_function

from __future__ import absolute_import
import argparse
import time
import os
import logging
import sys
import six
from six.moves import range

CONFIG = {
    'queue' : 'hpc4-3d',
    'cpusPerTask' : 1,
    'tasksPerJob' : 128,
    'jobCollectTimeout' : 15,
    'checkTime' : 5
}

def makeParser():
    parser = argparse.ArgumentParser(description="""Sample commands:
PYTHONPATH=../.. python th_client.py 'sleep 15; env; exit $SLURM_STEP_ID' -n 32
PYTHONPATH=../.. python th_client.py 'sleep 15; env' -n 32
Other parameters can be changed through CONFIG dict in th_client.py
""")
    parser.add_argument('command', help = "task's command")
    parser.add_argument('-d', '--directory', default='_task-', help = "task's directory prefix")
    parser.add_argument('-n', '--num-tasks', type=int, default="1",
                        help = "number of tasks to be started")
    return parser

def main():
    args = makeParser().parse_args()
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    #import everest_agent.task_handlers.slurm_th as TH
    import everest_agent.task_handlers.slurm_srun_th as TH

    th = TH.create(CONFIG)
    pids = {}
    states = {}
    for i in range(args.num_tasks):
        dirname = args.directory + str(i)
        try:
            os.mkdir(dirname)
        except OSError:
            pass
        #print dirname
        pids[i] = th.submit(args.command, dirname, {'KKK' : '123', 'LS_COLORS' : None})
        states[i] = None
    while True:
        final = True
        for i, pid in six.iteritems(pids):
            state = th.get_state(pid)
            ppp = pid
            if isinstance(ppp, tuple):
                if state == 'QUEUED':
                    ppp = None
                else:
                    ppp = ppp[0]['jobId']
            if state != states[i]:
                print(i, ppp, states[i], '->', state)
                states[i] = state
            if not state in ['CANCELED', 'DONE'] and not 'FAILED' in state:
                final = False
        if final:
            print('All tasks finished')
            break
        print('-----')
        time.sleep(5)

if __name__ == '__main__':
    main()
