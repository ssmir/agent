# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import json
import os
import math
import time
import logging

from tornado import gen
from tornado import web
from tornado.websocket import WebSocketHandler

from . import agent
from .constants import *
import six

logger = logging.getLogger('everest_agent.web')

def checkAuth(request, apc):
    if not 'Authorization' in request.headers:
        raise web.HTTPError(403)
    auth = request.headers['Authorization']
    spl = auth.split()
    if len(spl) != 2:
        logger.info('Rejecting %s with bad Authorization header' % request.remote_ip)
        raise web.HTTPError(403)
    if spl[0] == 'EverestAgentClientToken':
        authSpl = spl[1].split(':')
        if len(authSpl) != 2:
            logger.info('Rejecting %s with Authorization header having only one component' %
                        request.remote_ip)
            raise web.HTTPError(403)
        authToken = authSpl[1]
        clientId = authSpl[0]
        if authToken in apc.config.get('clientTokens'):
            return authToken, clientId
        logger.info('Rejecting token of client %s from %s' %
                    (clientId, request.remote_ip))
    raise web.HTTPError(403)

class AgentInfoHandler(web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.finish(json.dumps(self.application.apc.getAgentInfo()))

class ResourceInfoHandler(web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.finish(json.dumps(self.application.apc.getResourceInfo()))

def time2iso(t):
    frac = t - math.floor(t)
    td = time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(t))
    return '%s.%03dZ' % (td, int(frac * 1000))

class TaskListHandler(web.RequestHandler):
    def get(self):
        result = []
        for task in self.application.apc.tasks.values():
            result.append({'id' : task['id'],
                           'state' : task['state'],
                           'name' : task['name'],
                           'description' : task['description'],
                           'submitUTC': time2iso(task['processTimeStart']),
                           'clientId' : task['clientId']
                       })
        self.set_header('Content-Type', 'application/json')
        self.finish(json.dumps(result))

class TaskInfoHandler(web.RequestHandler):
    SUPPORTED_METHODS = ('DELETE', 'GET')

    INCLUDE_FIELDS = ['state', 'description', 'id', 'name', 'dir',
                      'command', 'inputData', 'deleteURIs', 'outputData'] + agent.ALL_ATTRS

    def get(self, taskId):
        if not taskId in self.application.apc.tasks:
            raise web.HTTPError(404)
        task = self.application.apc.tasks[taskId]
        out = {}
        for k, v in six.iteritems(task):
            if k in self.INCLUDE_FIELDS:
                out[k] = v
        out['submitUTC'] = time2iso(task['processTimeStart'])
        self.finish(out)

    def delete(self, taskId):
        if not taskId in self.application.apc.tasks:
            raise web.HTTPError(404)
        self.application.apc.deleteTask(taskId)
        self.set_status(202)
        self.finish()

class DownloadsHandler(web.RequestHandler):
    SUPPORTED_METHODS = ('DELETE', 'GET', 'HEAD')

    def initialize(self, apc):
        self.apc = apc

    def checkArgs(self, method, category, fname):
        checkAuth(self.request, self.apc)
        if category == 'upload':
            path = self.apc.cache.getFile(fname)
            if not path:
                raise web.HTTPError(404)
            return path
        if method == 'DELETE':
            raise web.HTTPError(403)
        if category != 'tasks':
            raise web.HTTPError(404)
        categoryDir = self.apc.tasksDir
        path = os.path.normpath(os.path.join(categoryDir, fname))
        if not os.path.dirname(path).startswith(categoryDir):
            raise web.HTTPError(403)
        if not os.path.isfile(path):
            raise web.HTTPError(404)
        return path

    @gen.coroutine
    def get(self, category, fname):
        path = self.checkArgs('GET', category, fname)
        self.set_header('Content-Length', str(os.path.getsize(path)))
        with open(path, 'rb') as f:
            while True:
                buf = f.read(FILE_CHUNK_SIZE)
                self.write(buf)
                yield self.flush()
                if not buf:
                    self.finish()
                    break

    def head(self, category, fname):
        path = self.checkArgs('HEAD', category, fname)
        self.set_header('Content-Length', str(os.path.getsize(path)))
        self.finish()

    def delete(self, category, fname):
        # delete uploaded/cached files only
        self.checkArgs('DELETE', category, fname)
        self.apc.cache.dropFile(fname)
        self.set_status(204)
        self.finish()

@web.stream_request_body
class UploadHandler(web.RequestHandler):
    def initialize(self, apc):
        self.apc = apc

    def prepare(self):
        checkAuth(self.request, self.apc)
        self.f = self.apc.cache.putFile()

    def data_received(self, data):
        self.f.write(data)

    def post(self):
        sha1, path = self.f.close()
        self.set_status(201)
        self.set_header('Location', '/agent/upload/%s' % sha1)
        self.finish()

class CheckFileHandler(web.RequestHandler):
    SUPPORTED_METHODS = ('HEAD', 'GET')

    def initialize(self, apc):
        self.apc = apc

    def impl(self):
        checkAuth(self.request, self.apc)
        sha1 = self.get_query_argument('sha1')
        path = self.apc.cache.getFile(sha1)
        if not path:
            raise web.HTTPError(404)
        self.set_status(301)
        self.set_header('Location', '/agent/upload/%s' % sha1)
        self.finish()

    def head(self):
        self.impl()

    def get(self):
        self.impl()

class WSAgentHandler(WebSocketHandler):
    def initialize(self, apc):
        self.apc = apc
        self.hasError = True

    def select_subprotocol(self, subprotocols):
        token, clientId = checkAuth(self.request, self.apc)
        self.clientId = clientId + ':' + token
        self.safeClientId = clientId + ':' + self.request.remote_ip

        if not PROTOCOL_VERSION in subprotocols:
            return None
        return PROTOCOL_VERSION

    def open(self):
        self.hasError = not self.apc.onConnectionOpen(
            self.clientId, self.safeClientId, self)
        if self.hasError:
            logger.info('Rejecting client %s with credentials of already connected client' %
                        self.safeClientId)
            self.close(1008, 'Connection with the specified clientId is already established')
            return
        logger.info('Accepted client %s' % self.safeClientId)

    def on_close(self):
        if not self.hasError:
            self.apc.onConnectionClose(self.clientId, self)

    def on_message(self, msg):
        if self.hasError:
            return
        if msg is None:
            self.apc.onConnectionClose(self.clientId, self)
            return
        self.apc.onMessage(self.clientId, self.safeClientId, self, msg)

    def on_pong(self, data):
        self.apc.resetPongTimeout(self.clientId)

def makeControlApp(config, apc):
    if not config.useControlAPI():
        return

    app = web.Application([
        (r"/control/tasks", TaskListHandler),
        (r"/control/tasks/(.+)", TaskInfoHandler),
        (r"/control/agent", AgentInfoHandler),
        (r"/control/resource", ResourceInfoHandler),
    ])
    app.apc = apc
    app.listen(config.get('controlPort'), 'localhost')
    return app

def makeMainServer(config, apc):
    if not config.usePassiveMode():
        return

    app = web.Application([
        (r"/agent/ws", WSAgentHandler, {'apc' : apc}),
        (r"/agent/(tasks)/(.+)", DownloadsHandler, {'apc' : apc}),
        (r"/agent/upload", UploadHandler, {'apc' : apc}),
        (r"/agent/(upload)/(.+)", DownloadsHandler, {'apc' : apc}),
        (r"/agent/filecheck", CheckFileHandler, {'apc' : apc})
    ])
    app.listen(config.get('agentPort'), max_body_size = config.get('maxBodySize'))
    return app
