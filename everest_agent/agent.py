#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import os
import tarfile
import zipfile
from contextlib import closing # For closing tarfiles in Python 2.6
import time
import fnmatch
import shutil
import struct
import json
import subprocess
import platform
import re
import random
import sys
import logging
import string
import locale
from multiprocessing import Pool
from collections import deque
import traceback
import signal

import tornado
from tornado import gen
from tornado import tcpserver
from tornado import iostream
from tornado import ioloop
from tornado import concurrent
from tornado.websocket import websocket_connect
from tornado.httpclient import AsyncHTTPClient

from everest_agent.exceptions import TaskException
from everest_agent import utils
from everest_agent.constants import *
from everest_agent import cache
import six
from six.moves import range

ALL_ATTRS = ['stageInBytes', 'stageInTime', 'waitTime', 'runTime', 'taskEndpointOut',
             'stageOutBytes', 'stageOutTime', 'outputDataOut', 'reason', 'processTime',
             'exitCode', 'errorCode']

STATE_ATTRS = {
    'ACCEPTED' : set(),
    'STAGED_IN' : set(['stageInBytes', 'stageInTime']),
    'QUEUED' : set(),
    'RUNNING' : set(['waitTime', 'taskEndpointOut']),
    'COMPLETED' : set(['runTime']),
    'STAGED_OUT' : set(['stageOutBytes', 'stageOutTime', 'outputDataOut']),
    'DONE' : set(ALL_ATTRS),
    'FAILED' : set(ALL_ATTRS),
    'CANCELED' : set(ALL_ATTRS),
    'DELETED' : set()
}

def getTaskLogger(taskId):
    return logging.getLogger('everest_agent.tasks.%s' % taskId)

def roundTime(t):
    return int(t)

def checkFilePath(path):
    if '/' in path:
        spl = path.split('/')
        if '..' in spl or spl[0] == '':
            return False
    elif '\\' in path:
        spl = path.split('\\')
        for s in spl:
            if '..' in s:
                return False
            for ss in s:
                if ss in [':<>|"?*']:
                    return False
    return True

def extractInputFileImpl(res_name, extractDir):
    if sys.version_info[0] == 2:
        extractDir = extractDir.encode('utf-8')
    if res_name.endswith('.zip'):
        try:
            with closing(zipfile.ZipFile(res_name, 'r')) as arch:
                for x in arch.namelist():
                    if not checkFilePath(x):
                        return "bad paths in archive"
                arch.extractall(path=extractDir)
        except RuntimeError:
            return "unable to unpack"
    elif res_name.endswith('.tar.gz'):
        try:
            with closing(tarfile.open(res_name,'r:gz')) as arch:
                for x in arch:
                    if not checkFilePath(x.name):
                        return "bad paths in archive"
                arch.extractall(path=extractDir)
        except tarfile.TarError:
            return "unable to unpack"
    else:
        return "unsupported archive type"
    return ''

def longrunWrapper(*args, **kw):
    if not 'proc' in kw:
        return 'error', 'proc not set'
    try:
        return 'ok', kw['proc'](*args)
    except:
        return 'error', traceback.format_exc()

def poolAsync(pool, proc, args):
    future = tornado.concurrent.Future()
    def callback(result_and_data):
        (result, data) = result_and_data
        if result == 'error':
            future.set_exception(TaskException(data))
        elif result == 'ok':
            future.set_result(data)
        else:
            assert(False)
    pool.apply_async(longrunWrapper, args, {'proc' : proc}, callback)
    return future

def readEndpointFile(path):
    try:
        with open(path, 'r') as f:
            data = f.read()
            spl = data.strip().split(':')
            return (spl[0], int(spl[1]))
    except IOError:
        pass
    except IndexError:
        pass
    return None

@gen.coroutine
def extractInputFile(pool, taskDir, path, extractDir):
    res_name = os.path.join(taskDir, path)
    result = yield poolAsync(pool, extractInputFileImpl, [res_name, extractDir])
    if result:
        raise TaskException('Failed to extract %s: %s' % (path, result))

def compressFiles(taskDir, file_name, files):
    with closing(tarfile.open(os.path.join(taskDir, file_name), 'w:gz')) as arch:
        for f in files:
            arch.add(os.path.join(taskDir,f),arcname=f)
    return ''

@gen.coroutine
def compressAndSendFiles(pool, taskDir, parcel, files, logger, dirName,
                         maxBodySize, connectTimeout, requestTimeout):
    file_name = parcel['pack']
    yield poolAsync(pool, compressFiles, [taskDir, file_name, files])
    for f in files:
        logger.debug('Added %s to archive' % f)
    logger.debug('Output archive %s written' % file_name)
    file_uri = '/agent/tasks/%s/%s' % (dirName, file_name)
    result = file_uri
    if 'uri' in parcel:
        result = sendFile(pool, taskDir, parcel, file_name, logger, dirName,
                          maxBodySize, connectTimeout, requestTimeout)
    raise gen.Return(result)

def sendFile(pool, taskDir, parcel, file_name, logger, dirName,
             maxBodySize, connectTimeout, requestTimeout):
    try:
        headers = {'Content-Type' : 'application/x-tar'}
        if 'auth' in parcel:
            headers['Authorization'] = parcel['auth']
        if parcel['uri'].endswith('/'):
            method = 'POST'
        else:
            method = 'PUT'
        return utils.uploadFileRequest(
            parcel['uri'], os.path.join(taskDir, file_name),
            method=method, headers=headers, request_timeout=requestTimeout,
            connect_timeout=connectTimeout)
    except Exception as e:
        logger.exception("Unable to upload output to server")
        raise TaskException("Unable to upload %s to %s: %s" % (
            file_name, parcel['uri'], e))

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

class DogTemplate(string.Template):
  delimiter = "@"

class Agent(tcpserver.TCPServer):
    def __init__(self,config):
        tcpserver.TCPServer.__init__(self)

        self.logger = logging.getLogger("everest_agent.agent")
        self.config = config
        self.check_time = self.config.get('checkTime')
        self.logger.debug("Check time %g" % self.check_time)
        self.tasksDir = self.config.get('tasksDir')
        self.cache = cache.Cache(self.config.get('cacheDir'),
                                 self.config.get('maxCacheSize'))
        self.pool = Pool(self.config.get('utilityPoolSize'), init_worker)
        assert self.config.get('taskDirCleanupPeriod') >= 1

        THType = self.config.get('taskHandler')['type']
        try:
            THModule = __import__('everest_agent.task_handlers.%s_th' % THType,
                                  globals(), locals(), ['create'])
        except ImportError:
            self.logger.error('Unknown task handler type: %s' % THType)
            sys.exit(1)
        self.logger.info("Initializing %s task handler" % THType)
        self.TH = THModule.create(self.config.getTaskHandlerConf())

        self.whitelist = []
        if not self.config.get('whitelist') is None:
            for line in self.config.get('whitelist'):
                try:
                    command_regexp = re.compile(line.rstrip('\n\r'))
                    self.whitelist.append(command_regexp)
                except re.error:
                    self.logger.error("'%s' in allowedCommands list is not a correct regular expression" % line)
                    sys.exit(1)
        else:
            command_regexp = re.compile(".*")
            self.whitelist.append(command_regexp)

        if sys.platform == "win32":
            import ctypes
            code_page = "CP%d" % ctypes.windll.kernel32.GetOEMCP()
        else:
            code_page = sys.stdout.encoding or sys.stderr.encoding or locale.getdefaultlocale()[1]

        self.agentInfo = {
            'version' : AGENT_VERSION,
            'pythonVersion' : platform.python_version(),
            'system' : list(platform.uname()),
            'stdout-encoding': code_page
        }

        AsyncHTTPClient.configure(None, defaults = dict(validate_cert=False))

        self.tasks = {}
        self.task_queue = deque()
        self.connections = {}

        if self.config.get("keepTaskDir"):
            self.taskDirCleanupHandle = ioloop.IOLoop.current().call_later(
                self.config.get('taskDirCleanupPeriod'), lambda: self.cleanupTasksDir())


    def shutdown(self):
        for task in self.tasks.values():
            if 'pid' in task:
                self.TH.cancel(task['pid'])
        if self.config.get("keepTaskDir"):
            ioloop.IOLoop.current().remove_timeout(self.taskDirCleanupHandle)
        self.pool.terminate()
        self.pool.join()
        self.cache.shutdown()

    def startTaskMessageServer(self):
        self.logger.info("Starting task message server listening on %s:%d" % (
            self.config.get('taskAddress'), self.config.get('taskPort')))
        self.listen(self.config.get('taskPort'), self.config.get('taskAddress'))

    def getAllTaskPids(self):
        return [task['pid'] for task in self.tasks.values() if 'pid' in task]

    def getAgentInfo(self):
        return self.agentInfo

    def getResourceInfo(self):
        pids = self.getAllTaskPids()
        result = {
            'type' : self.TH.get_type(),
            'slots' : {'total' : self.config.get('maxTasks')},
            'tasks' : {'total' : len(self.task_queue) + len(pids),
                       'maxRunning' : self.config.get('maxTasks')}
        }

        if result['type'] in ['docker']:
            result['tasks']['running'] = len(pids)
            result['slots']['free'] = result['slots']['total'] - result['tasks']['running']
        elif result['type'] in ['slurm_srun']:
            result['tasks']['running'] = len(pids)
            result['slots']['total'] = self.TH.get_slots()
            result['slots']['free'] = self.TH.get_free_slots()
        else:
            running = len([p for p in pids if self.TH.get_state(p) in ['RUNNING', 'TIMEOUT']])
            result['tasks']['running'] = running
            result['slots']['total'] = self.TH.get_slots()
            result['slots']['free'] = self.TH.get_free_slots()

        # resource attributes
        result['attributes'] = self.config.get('attributes')

        return result

    @gen.coroutine
    def handle_stream(self, stream, address):
        self.logger.debug('New task connection from %s established' % str(address))
        try:
            sizeRaw = yield stream.read_bytes(4)
            size, = struct.unpack('>I', sizeRaw)
            self.logger.debug('Receiving first task connection message of %d bytes' % size)
            data = yield stream.read_bytes(size)
            protocol, taskId = struct.unpack('b'+str(size-1)+'s', data)
            taskId = taskId.decode()
            self.logger.info('Task %s connected with protocol %d' % (taskId, protocol))

            if not taskId in self.tasks:
                raise RuntimeError('Task not found')
            task = self.tasks[taskId]
            if protocol == 0:
                task['stream'] = stream
                task['stream_protocol'] = protocol
                while task['tm_queue']:
                    p, msg = task['tm_queue'].popleft()
                    self.sendTaskMessage(task, p, msg)
                while not stream.closed():
                    sizeRaw = yield stream.read_bytes(4)
                    size, = struct.unpack('>I', sizeRaw)
                    data = yield stream.read_bytes(size)
                    task['logger'].debug('Forwarding %d bytes from task' % size)
                    if not task['clientId'] in self.connections:
                        raise RuntimeError('No connection to the client')
                    self.sendMessage(self.connections[task['clientId']],
                                     'TASK_MESSAGE', taskId, protocol, data.decode())
            else:
                raise RuntimeError('Unsupported protocol version')
        except iostream.StreamClosedError:
            try:
                task['logger'].info('Task connection closed')
                del task['stream']
                del task['stream_protocol']
            except:
                self.logger.info('Task connection closed')
        except Exception as e:
            self.logger.exception('Closing task connection')
            try:
                del task['stream']
                del task['stream_protocol']
            except:
                pass
            if not stream.closed():
                msg = str(e).encode()
                yield stream.write(struct.pack('>I', len(msg)))
                yield stream.write(msg)
                stream.close()

    @gen.coroutine
    def ws_client_loop(self):
        while True:
            self.logger.info('Connecting to server %s' % self.config.get('serverURI'))
            try:
                req = tornado.httpclient.HTTPRequest(
                    self.config.get('serverURI'), validate_cert=False,
                    headers = {'Authorization' : 'EverestAgentToken %s' % self.config.get('agentToken'),
                               'Sec-WebSocket-Protocol' : 'v1.agent.everest'})
                conn = yield websocket_connect(req)
                if not 'Sec-WebSocket-Protocol' in conn.headers:
                    conn.close(1002)
                    raise RuntimeError('Server did not accept our protocol version')
                if conn.headers['Sec-WebSocket-Protocol'] != PROTOCOL_VERSION:
                    conn.close(1002)
                    raise RuntimeError('Server returned wrong protocol version')
            except Exception as e:
                self.logger.debug(str(e))
                yield gen.sleep(random.randint(2, 10))
                continue

            try:
                self.onConnectionOpen(None, None, conn)
            except:
                self.logger.exception("Failed to set up connection")
                self.onConnectionClose(None, conn)
                continue

            while True:
                self.logger.debug("Waiting for a message from server")
                msg = yield conn.read_message()
                if msg is None:
                    self.onConnectionClose(None, conn)
                    yield gen.sleep(random.randint(2, 10))
                    break
                try:
                    self.onMessage(None, None, conn, msg)
                except:
                    self.logger.exception('Failed to process message from server: %s' % msg)

    def onConnectionOpen(self, clientId, safeClientId, conn):
        if clientId in self.connections:
            return False

        try:
            resinfo = self.getResourceInfo()
        except:
            self.logger.exception("getResourceInfo() failed")
            resinfo = {}
        if not self.connections:
            self.lastResourceInfo = resinfo
            self.resinfo_handle = ioloop.IOLoop.current().call_later(
                self.config.get('resourceInfoPeriod'), self.sendResourceInfo)

        handle = ioloop.IOLoop.current().call_later(
            self.config.get('pingPeriod'), lambda: self.sendPing(clientId))

        self.connections[clientId] = {
            'clientId' : clientId,
            'safeClientId' : safeClientId,
            'conn' : conn,
            'ping_handle' : handle
        }

        self.sendMessage(self.connections[clientId], 'WELCOME',
                         self.getAgentInfo(), resinfo)

        if clientId is None:
            conn.on_pong = self.resetPongTimeout
            self.logger.info("Connected to server")
        else:
            self.logger.info("Client %s connected" % safeClientId)

        hangingTasks = [task for task in self.tasks.values() if task['clientId'] == clientId]
        if hangingTasks:
            self.logger.info("Client %s has %d hanging tasks. Sending current task states" % \
                             (safeClientId, len(hangingTasks)))
            for task in hangingTasks:
                self.sendTaskState(task)
        return True

    def onConnectionClose(self, clientId, conn):
        self.cleanupConnection(clientId)

    def cleanupConnection(self, clientId):
        if not clientId in self.connections:
            return

        safeClientId = self.connections[clientId]['safeClientId']

        ioloop.IOLoop.current().remove_timeout(self.connections[clientId]['ping_handle'])
        if 'pong_handle' in self.connections[clientId]:
            ioloop.IOLoop.current().remove_timeout(self.connections[clientId]['pong_handle'])
        del self.connections[clientId]

        if not self.connections:
            ioloop.IOLoop.current().remove_timeout(self.resinfo_handle)

        if clientId is None:
            self.logger.info("Closed connection to server")
        else:
            self.logger.info("Client %s connection closed" % safeClientId)

    def onMessage(self, clientId, safeClientId, conn, m):
        self.logger.debug("Received message from %s: %s" % (safeClientId, m))
        self.resetPongTimeout(clientId)
        msg = json.loads(m)
        if msg[0] == 'TASK_SUBMIT':
            self.handleSubmit(clientId, msg[1], msg[2], msg[3])
        elif msg[0] == 'TASK_CANCEL':
            self.cancelTask(msg[1])
        elif msg[0] == 'HELLO':
            pass
        elif msg[0] == 'TASK_DELETE':
            self.deleteTask(msg[1], clientId)
        elif msg[0] == 'TASK_MESSAGE':
            self.handleTaskMessage(msg[1], msg[2], msg[3])
        elif msg[0] == 'STAGEIN':
            ioloop.IOLoop.current().add_future(
                gen.moment, lambda f: self.handleStageIn(msg[1], msg[2], clientId))
        elif msg[0] == 'STAGEOUT':
            ioloop.IOLoop.current().add_future(
                gen.moment, lambda f: self.handleStageOut(msg[1], msg[2], clientId))
        elif msg[0] == 'LISTDIR':
            ioloop.IOLoop.current().add_future(
                gen.moment, lambda f: self.handleListDir(msg[1], clientId))
        elif msg[0] == 'SHUTDOWN':
            ioloop.IOLoop.current().stop()

    @gen.coroutine
    def handleStageIn(self, taskId, inputData, clientId):
        result, reason = 'DONE', 'OK'
        if not taskId in self.tasks:
            result, reason = 'FAILED', 'Task not found'
        else:
            task = self.tasks[taskId]
            try:
                yield self.doStageIn(self.pool, inputData, task['dir'], task['logger'])
            except Exception as e:
                task['logger'].exception('STAGEIN failed')
                result, reason = 'FAILED', str(e)
        self.sendMessageT(clientId, 'STAGEIN_RESP', taskId, result, reason)

    @gen.coroutine
    def handleStageOut(self, taskId, outputData, clientId):
        result, reason, out = 'FAILED', '', []
        if not taskId in self.tasks:
            reason = 'Task not found'
        else:
            task = self.tasks[taskId]
            try:
                ret, err = yield self.doStageOut(self.pool, outputData, task['dir'],
                                                 task['logger'], task['dirName'])
                out = ret['outputDataOut']
                if err:
                    reason = err['reason']
                else:
                    result = 'DONE'
            except Exception as e:
                task['logger'].exception('STAGEOUT failed')
                reason = str(e)
        self.sendMessageT(clientId, 'STAGEOUT_RESP', taskId, result, out, reason)

    @gen.coroutine
    def handleListDir(self, taskId, clientId):
        result = {}
        if taskId in self.tasks:
            task = self.tasks[taskId]
            try:
                for root, dirs, files in os.walk(task['dir']):
                    for filename in files:
                        sha1 = utils.sha1(os.path.join(root, filename))
                        troot = root.replace(task['dir'], "")
                        if troot.startswith(os.sep):
                            troot = troot.replace(os.sep, "", 1)
                        result[os.path.join(troot, filename).replace(os.sep, '/')] = {
                            'sha1' : sha1}
            except:
                task['logger'].exception('LISTDIR failed')
                result = {}
        self.sendMessageT(clientId, 'LISTDIR_RESP', taskId, result)

    def handleTaskMessage(self, taskId, protocol, msg):
        if not taskId in self.tasks:
            self.logger.warn('Got TASK_MESSAGE for task %s but have no entry for it' % taskId)
            return
        task = self.tasks[taskId]
        if not 'stream' in task:
            task['logger'].warn('Got TASK_MESSAGE but no stream found. Putting to a queue.')
            task['tm_queue'].append((protocol, msg.encode()))
            return
        self.sendTaskMessage(task, protocol, msg.encode())

    def sendTaskMessage(self, task, protocol, msg):
        if protocol != task['stream_protocol']:
            task['logger'].warn('Got TASK_MESSAGE with wrong protocol')
            return
        if protocol == 0:
            task['stream'].write(struct.pack('>I', len(msg)))
            task['stream'].write(msg)
        else:
            assert(False)

    def sendPing(self, clientId):
        conn = self.connections[clientId]
        self.logger.debug('Sending ping to client %s' % conn['safeClientId'])

        try:
            if clientId is None:
                conn['conn'].protocol.write_ping(six.b(''))
            else:
                conn['conn'].ping(six.b(''))

            conn['ping_handle'] = ioloop.IOLoop.current().call_later(
                self.config.get('pingPeriod'), lambda: self.sendPing(clientId))
            if not 'pong_handle' in conn:
                conn['pong_handle'] = ioloop.IOLoop.current().call_later(
                    self.config.get('pongTimeout'), lambda: self.onPongTimeout(clientId))
        except Exception as e:
            self.logger.exception("Can't send a ping, connection is lost. Trying to reconnect")
            conn['conn'].close()
            self.cleanupConnection(conn['clientId'])

    def resetPongTimeout(self, clientId):
        if clientId == '':
            clientId = None
        try:
            if not clientId in self.connections:
                return
            conn = self.connections[clientId]
            self.logger.debug('Resetting pong timeout for client %s' % conn['safeClientId'])
            if 'pong_handle' in conn:
                ioloop.IOLoop.current().remove_timeout(conn['pong_handle'])
            conn['pong_handle'] = ioloop.IOLoop.current().call_later(
                self.config.get('pongTimeout'), lambda: self.onPongTimeout(clientId))
        except Exception:
            self.logger.exception('Error resetting pong timeout')
            raise

    def onPongTimeout(self, clientId):
        self.logger.debug('No pongs from client %s for %f seconds. Dropping connection' %
                          (self.connections[clientId]['safeClientId'], self.config.get('pongTimeout')))
        self.connections[clientId]['conn'].close(1000, "Pong timeout")
        self.cleanupConnection(clientId)

    def deleteTask(self, taskId, clientId = None):
        if clientId is None and taskId in self.tasks:
            clientId = self.tasks[taskId]['clientId']
        ioloop.IOLoop.current().add_future(
            gen.moment, lambda f: self.deleteTaskImpl(clientId, taskId))

    @gen.coroutine
    def deleteTaskImpl(self, clientId, taskId):
        if not taskId in self.tasks:
            if clientId in self.connections:
                self.sendMessage(self.connections[clientId], 'TASK_STATE', taskId,
                                 'DELETED', {})
            self.logger.info("Deleting task %s - task not found" % taskId)
            return
        task = self.tasks[taskId]
        if not task['state'] in ['FAILED', 'CANCELED', 'DONE']:
            self.cancelTask(taskId)
            yield task['worker_done']
        futures = []
        for uri, auth in task['deleteURIs']:
            task['logger'].debug("Deleting input %s" % uri)
            if uri[0] == '/':
                if not self.removeRelativeURIPath(uri):
                    task['logger'].debug("Local input file %s not found" % uri)
                continue
            client = AsyncHTTPClient(max_buffer_size=self.config.get('maxBodySize'))
            headers = {}
            if auth:
                headers['Authorization'] = auth
            futures.append(client.fetch(uri, method='DELETE', headers=headers,
                                        request_timeout=self.config.get('requestTimeout'),
                                        connect_timeout=self.config.get('connectTimeout')))
        try:
            yield futures
        except Exception:
            task['logger'].exception('DELETE failed for some files during task deletion')
        if not self.config.get("keepTaskDir"):
            try:
                shutil.rmtree(task['dir'])
            except:
                task['logger'].exception('Failed to delete task directory')
        del self.tasks[task['id']]
        self.sendTaskState(task, 'DELETED')
        self.logger.info("Task %s deleted" % taskId)

    def getDirSize(self, start_path):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size

    def cleanupTasksDir(self):
        try:
            liveTasks = set()
            for k, v in six.iteritems(self.tasks):
                liveTasks.add(k)
            dirSizes = []
            for d in os.listdir(self.tasksDir):
                path = os.path.join(self.tasksDir, d)
                if os.path.isdir(path):
                    stat = os.stat(path)
                    dirSizes.append((float(stat.st_mtime), d, path, self.getDirSize(path)))
            totalSize = sum([s[3] for s in dirSizes])
            liveSize = sum([s[3] for s in dirSizes if s[1] in liveTasks])
            self.logger.info("cleanupTasksDir: totalSize=%d totalCount=%d liveSize=%d maxSize=%d" % (
                totalSize, len(dirSizes), liveSize, self.config.get('taskDirSize')))
            if totalSize <= self.config.get('taskDirSize'):
                return

            nRemoved = 0
            for s in sorted(dirSizes):
                if s[1] in liveTasks:
                    continue
                if totalSize <= self.config.get('taskDirSize'):
                    break
                self.logger.debug("cleanupTasksDir: removing task %s, size %d" % (s[1], s[3]))
                try:
                    shutil.rmtree(s[2])
                except:
                    self.logger.exception('Failed to delete task directory %s' % s[2])
                totalSize -= s[3]
                nRemoved += 1
            if nRemoved:
                self.logger.info("cleanupTasksDir: %d old task dirs removed, new totalSize=%d" % (
                    nRemoved, totalSize))
        finally:
            self.taskDirCleanupHandle = ioloop.IOLoop.current().call_later(
                self.config.get('taskDirCleanupPeriod'), lambda: self.cleanupTasksDir())

    def cancelTask(self, taskId):
        if not taskId in self.tasks:
            self.logger.info("Canceling task %s - task not found." % taskId)
            return
        task = self.tasks[taskId]
        if 'pid' in task:
            self.logger.info("Canceling task %s with pid %s" %
                             (taskId, str(task['pid'])))
            self.TH.cancel(task['pid'])
        else:
            self.logger.info("Canceling task %s without pid in %s state" % (
                taskId, task['state']))
            task['reason'] = 'Task canceled in %s state' % task['state']
            task['state'] = 'CANCELED'
            task['processTime'] = roundTime(time.time() - task['processTimeStart'])
            self.sendTaskState(task)

    def handleSubmit(self, clientId, taskId, task, context):
        task['clientId'] = clientId
        task['processTimeStart'] = time.time()
        task['id'] = taskId
        logger = getTaskLogger(task['id'])
        task['logger'] = logger
        if not 'name' in task:
            task['name'] = ''
        if not 'description' in task:
            task['description'] = ''

        if taskId in self.tasks:
            task['state'] = 'FAILED'
            task['reason'] = 'Task with the same ID is already registered'
            task['processTime'] = 0
            self.sendTaskState(task)
            logger.info('%s FAILED: %s' % (task['id'], task['reason']))
            return

        task['worker_done'] = tornado.concurrent.Future()

        # replace resource attributes inside the command
        task['command'] = DogTemplate(task['command']).safe_substitute(self.config.get('attributes'))

        allowed = False
        for regexp in self.whitelist:
            logger.debug("Matching command %s against pattern %s" %
                         (task['command'], regexp.pattern))
            if regexp.match(task['command']):
                allowed = True
                break
        if not allowed:
            task['state'] = 'FAILED'
            task['reason'] = 'Specified command is not allowed'
            task['errorCode'] = 2
            task['processTime'] = roundTime(time.time() - task['processTimeStart'])
            self.sendTaskState(task)
            logger.info('%s FAILED: Specified command is not allowed' % task['id'])
            return

        # check proxy settings
        if task.get('taskEndpoint', {}).get('enable', False) and \
           task['taskEndpoint'].get('proxy', False):
            if not self.config.get('proxyEnabled') or \
               not self.config.get('proxyFrontendURI') or \
               not self.config.get('proxyBackendToken'):
                task['state'] = 'FAILED'
                task['reason'] = 'Agent does not accept proxified task endpoints'
                task['processTime'] = roundTime(time.time() - task['processTimeStart'])
                self.sendTaskState(task)
                logger.info('%s FAILED: proxy config parameters not set' % task['id'])
                return

        # task environment variables
        if not 'environment' in task:
            task['environment'] = {}
        else:
            ready_environment = {}
            # environment variables and resource attributes substitution
            for k, v in six.iteritems(task['environment']):
                v = v or ""
                if v:
                  v = string.Template(v).safe_substitute(os.environ)
                  v = DogTemplate(v).safe_substitute(self.config.get('attributes'))
                  v = DogTemplate(v).safe_substitute({"PATH_SEP": os.pathsep})
                if isinstance(k, six.text_type):
                  k = k.encode(locale.getdefaultlocale()[1] or 'ascii', "replace")
                if isinstance(v, six.text_type):
                  v = v.encode(locale.getdefaultlocale()[1] or 'ascii', "replace")
                ready_environment[k] = v
            task['environment'] = ready_environment

        self.sendTaskState(task, 'ACCEPTED')
        task['state'] = 'ACCEPTED'

        dirParts = []

        if 'user' in context:
            task['environment']['EVEREST_AGENT_TASK_USER'] = str(context['user'])
        # if 'user' in context:
        #     dirParts.append(context['user'])
        #
        # if 'application' in context:
        #     appName = context['application'].rstrip('/')
        #     appName = appName.split('/')[-1]
        #     dirParts.append(appName)

        dirParts.append(''.join(task['id'].split('/')))

        task['dirName'] = '-'.join(dirParts)
        task['dir'] = os.path.join(self.tasksDir, task['dirName'])

        ioloop.IOLoop.current().add_future(gen.moment, lambda f: self.TaskWorker(task))

    def waitForFreeSlot(self):
        future = tornado.concurrent.Future()
        self.task_queue.append(future)
        self.submitTask()
        return future

    def updateTaskState(self, task):
        ret = self.TH.get_state(task['pid'])
        if ret == 'TIMEOUT':
            return
        #getTaskLogger(task['id']).debug("Current state: %s" % ret)
        spl = ret.split(' ', 1)
        task['state'] = spl[0]
        if task['state'] == 'DONE':
            task['exitCode'] = 0
        elif task['state'] == 'FAILED' and len(spl) > 1 and spl[1].startswith('ExitCode='):
            task['exitCode'] = int(spl[1][len('ExitCode='):])
            task['errorCode'] = 1
        if task['state'] in ['FAILED', 'CANCELED']:
            if len(spl) == 1:
                task['reason'] = ''
            else:
                task['reason'] = spl[1]
        if task['state'] in ['COMPLETED', 'FAILED', 'CANCELED', 'DONE']:
            del task['pid']

    def getRelativeURIPath(self, uri):
        spl = uri.split('/')
        if spl[1] != 'agent':
            return ''
        if spl[2] == 'upload':
            return self.cache.getFile(spl[3])
        if spl[2] != 'tasks':
            return ''
        path = os.path.join(self.tasksDir, os.sep.join(spl[3:]))
        if not os.path.isfile(path):
            return ""
        return path

    def removeRelativeURIPath(self, uri):
        spl = uri.split('/')
        if spl[1] != 'agent':
            return False
        if spl[2] == 'upload':
            return self.cache.dropFile(spl[3])
        path = self.getRelativeURIPath(uri)
        if not path:
            return False
        os.remove(path)
        return True

    @gen.coroutine
    def doStageIn(self, pool, inputData, taskDir, logger):
        result = {
            'deleteURIs' : [],
            'stageInBytes' : 0
        }
        logger.info('Downloading input files')
        download_start = time.time()
        requests = []
        files = []
        wait_files = []
        for inp in inputData:
            if inp.get('delete', False):
                result['deleteURIs'].append((inp['uri'], inp.get('auth', '')))
            dest_path = os.path.join(taskDir, inp['path'])
            dest_dir = os.path.dirname(dest_path)
            # make sure dest dir exists
            if not os.path.isdir(dest_dir):
                try:
                    os.makedirs(dest_dir)
                except OSError:
                    if not os.path.isdir(dest_dir):
                        raise TaskException("Failed to create dir: %s" % dest_dir)

            if '/agent/tasks/' in inp['uri']:
                path = inp['uri'][inp['uri'].index('/agent/tasks'):]
                if self.getRelativeURIPath(path):
                    logger.debug('Detected local input file: %s -> %s', inp['uri'], path)
                    inp['uri'] = path
                else:
                    # the file is stored on another agent
                    # we reuse agent token as a client token for authentication
                    inp['auth'] = 'EverestAgentClientToken %s:%s' % ('agent', self.config.get('agentToken'))

            if inp['uri'][0] == '/':
                path = self.getRelativeURIPath(inp['uri'])
                if not path:
                    raise TaskException("Local input file %s not found" % inp['uri'])
                shutil.copyfile(path, dest_path)
                logger.debug('Copied local input file: %s -> %s', path, dest_path)
                continue
            elif 'sha1' in inp:
                path = self.cache.getFile(inp['sha1'])
                if path:
                    logger.debug('Cache hit for %s with hash %s' % (
                        inp['path'], inp['sha1']))
                    shutil.copyfile(path, dest_path)
                    continue
                future = self.cache.waitFile(inp['sha1'])
                if not future is None:
                    logger.debug('Waiting for %s with hash %s to be downloaded by another task' % (
                        inp['path'], inp['sha1']))
                    wait_files.append((dest_path, inp['sha1'], future))
                    continue
            f = self.cache.putFile(inp.get('sha1', ''))
            files.append((dest_path, inp.get('sha1', ''), f))
            headers = {}
            if 'auth' in inp:
                headers['Authorization'] = inp['auth']
            requests.append(tornado.httpclient.HTTPRequest(
                inp['uri'], streaming_callback=f.write, headers=headers,
                request_timeout=self.config.get('requestTimeout'),
                connect_timeout=self.config.get('connectTimeout')))
        try:
            yield self.fetchWithRetries(requests, logger)
        except TaskException as e:
            e.fail_msg = "Unable to download input: " + e.fail_msg
            if 'HTTP 599' in e.fail_msg:
                e.error_code = 3
            raise e
        except Exception as e:
            logger.exception("Unable to download input file")
            raise TaskException("Unable to download input file: %s" % e)
        finally:
            files = [(n, s, f.close()) for n, s, f in files]

        after_wait = yield [w[2] for w in wait_files]
        for i, r in enumerate(after_wait):
            files.append((wait_files[i][0], wait_files[i][1], r))

        for dest_path, shaOrig, (shaReal, path) in files:
            if shaOrig and shaReal != shaOrig:
                raise TaskException("Input file %s hash differs from downloaded %s != %s"
                                    % (dest_path, shaOrig, shaReal))

            shutil.copyfile(path, dest_path)
            result['stageInBytes'] += os.path.getsize(dest_path)

        # extract input archives
        futures = []
        for inp in inputData:
            if 'unpack' in inp:
                logger.debug('Extracting input archive %s' % inp['path'])
                futures.append(extractInputFile(
                    pool, taskDir, inp['path'],
                    os.path.join(taskDir, inp['unpack'])))
        try:
            yield futures
        except Exception as e:
            logger.exception("Failed to extract input archive")
            raise TaskException("Failed to extract input archive: %s" % e)

        result['stageInTime'] = roundTime(time.time() - download_start)
        raise gen.Return(result)

    @gen.coroutine
    def doStageOut(self, pool, outputData, taskDir, logger, dirName):
        result = {
            'outputDataOut' : [],
            'stageOutBytes' : 0,
            'stageOutTime' : 0.0
        }
        error = {}
        stageout_start = time.time()
        requests = []
        results = []
        paths = []
        for parcel in outputData:
            outputFiles = []
            for pattern in parcel['paths']:
                logger.debug('Trying to find %s among produced files' % pattern)
                files_to_upload = []
                for root, dirs, files in os.walk(taskDir):
                    root = root.replace(taskDir, "")
                    if root.startswith(os.sep):
                        root = root.replace(os.sep, "", 1)
                    for filename in files:
                        if fnmatch.fnmatch(os.path.join(root, filename), pattern):
                            files_to_upload.append(os.path.join(root, filename))
                if not len(files_to_upload):
                    # failing tasks with missing files is currently disabled as too restrictive
                    # TODO: fail task if required outputs are missing (client should pass the required flag)
                    #logger.debug('Failed to find %s' % pattern)
                    #error['state'] = 'FAILED'
                    #error['reason'] = 'Program has not produced expected file(s): %s' % pattern
                    continue
                logger.debug('%s found' % pattern)
                for f in files_to_upload:
                    logger.debug('Selecting %s for upload' % f)
                    file_uri = '/agent/tasks/%s/%s' % (dirName, f)
                    file_path = os.path.join(taskDir, f)
                    outputFiles.append({
                        'path': f,
                        'uri': file_uri,
                        'size': os.path.getsize(file_path),
                        'sha1': utils.sha1(file_path)
                    })
            if 'pack' in parcel:
                file_name = parcel['pack']
                request = yield compressAndSendFiles(
                    pool, taskDir, parcel, [f['path'] for f in outputFiles],
                    logger, dirName, self.config.get('maxBodySize'),
                    self.config.get('connectTimeout'), self.config.get('requestTimeout'))
                if isinstance(request, tornado.httpclient.HTTPRequest):
                    requests.append(request)
                    paths.append(file_name)
                else:
                    file_path = os.path.join(taskDir, file_name)
                    results.append({
                        'path': file_name,
                        'uri': request,
                        'size': os.path.getsize(file_path),
                        'sha1': utils.sha1(file_path)
                    })
                outputFiles = []

            elif 'uri' in parcel:
                file_name = parcel['paths'][0]
                request = sendFile(
                    pool, taskDir, parcel, file_name, logger, dirName,
                    self.config.get('maxBodySize'), self.config.get('connectTimeout'),
                    self.config.get('requestTimeout'))
                requests.append(request)
                paths.append(file_name)
                outputFiles = []

            result['outputDataOut'] += outputFiles

        try:
            responses = yield self.fetchWithRetries(requests, logger)
            for i, r in enumerate(responses):
                assert r.code == 200
                results.append({
                    'path': paths[i],
                    'uri': r.headers.get('Location', r.request.url),
                    'size': os.path.getsize(os.path.join(taskDir, paths[i]))
                })
        except TaskException as e:
            e.fail_msg = "Unable to upload output: " + e.fail_msg
            if 'HTTP 599' in e.fail_msg:
                e.error_code = 5
            raise e
        except Exception as e:
            logger.exception("Unable to upload output")
            raise TaskException("Unable to upload output: %s" % e)

        result['stageOutTime'] += roundTime(time.time() - stageout_start)
        result['outputDataOut'] += results
        for out in result['outputDataOut']:
            result['stageOutBytes'] += out['size']
        raise gen.Return((result, error))

    @gen.coroutine
    def fetchWithRetries(self, requests, logger):
        tries = 0
        results = [None for r in requests]
        mapping = dict([(i, i) for i in range(len(requests))])
        maxTries = self.config.get('requestTries')
        while requests and tries < maxTries:
            client = AsyncHTTPClient(max_buffer_size = self.config.get('maxBodySize'))
            futures = [client.fetch(r, raise_error=False) for r in requests]
            requests = []
            responses = yield futures
            newMapping = {}
            for i, r in enumerate(responses):
                if r.code == 200:
                    results[mapping[i]] = r
                    continue
                elif r.code == 599 and tries + 1 < maxTries:
                    logger.info("%s, method %s, url %s" % (
                        r.error, r.request.method, r.request.url))
                    rtimeout = random.uniform(
                        self.config.get('requestTimeout'),
                        self.config.get('requestTimeout') * 2.**(tries+1))
                    ctimeout = random.uniform(
                        self.config.get('connectTimeout'),
                        self.config.get('connectTimeout') * 2.**(tries+1))
                    r.request.request_timeout = rtimeout
                    r.request.connect_timeout = ctimeout
                    newMapping[len(requests)] = mapping[i]
                    requests.append(r.request)
                    continue
                logger.error("%s, method %s, url %s. Response body: %s" % (
                    r.error, r.request.method, r.request.url, r.body))
                raise TaskException("%s for %s" % (r.error, r.request.url))
            tries += 1
            mapping = newMapping
        raise gen.Return(results)

    @gen.coroutine
    def TaskWorker(self, task):
        self.tasks[task['id']] = task
        task['tm_queue'] = deque()
        logger = getTaskLogger(task['id'])
        try:
            logger.info("Setting up task's environment")
            try:
                os.makedirs(task['dir'])
            except OSError:
                logger.warning("Task's directory %s already exists" % task['dir'])

            task['deleteURIs'] = []
            if len(task['inputData']) > 0:
                ret = yield self.doStageIn(self.pool, task['inputData'], task['dir'], logger)
                task.update(ret)
                if task['state'] == 'CANCELED':
                    return
                self.sendTaskState(task, 'STAGED_IN')

            logger.info('Waiting for a free slot')
            yield self.waitForFreeSlot()
            if task['state'] == 'CANCELED':
                return

            # set executable flag (if executable in task dir)
            if task['command'].startswith('./') and os.name != 'nt':
                executable = task['command'].split()[0][2:]
                subprocess.call(['chmod', 'u+x', task['dir'] + '/' + executable])

            # set task environment
            env = task['environment']
            if self.config.useTaskProtocol():
                env.update({'EVEREST_AGENT_PORT' : str(self.config.get('taskPort')),
                            'EVEREST_AGENT_ADDRESS' : str(self.config.get('taskAddress')),
                            'EVEREST_AGENT_TASK_ID' : str(task['id'])})

            # resource requirements
            if 'resourceRequirements' in task:
                requirements = task['resourceRequirements']
            else:
                requirements = None

            logger.info('Starting the command')
            submit_start = time.time()
            pid = self.TH.submit(task['command'], os.path.abspath(task['dir']), env, requirements)
            #print pid
            if not pid:
                raise TaskException("Unable to start worker process")
            task['pid'] = pid

            last_check_time = time.time()
            self.updateTaskState(task)

            if task['state'] == 'QUEUED':
                self.sendTaskState(task, 'QUEUED')
                while (task['state'] == 'QUEUED'):
                    yield gen.sleep(self.check_time)
                    last_check_time = time.time()
                    self.updateTaskState(task)

            if task['state'] == 'RUNNING':
                run_start = time.time()
                task['waitTime'] = roundTime(run_start - submit_start)
                if task.get('taskEndpoint', {}).get('enable', False):
                    taskEndpoint = None
                    endpointPath = os.path.join(task['dir'], 'task.endpoint')
                    logger.debug('Waiting for task endpoint in %s' % endpointPath)
                    while task['state'] == 'RUNNING':
                        taskEndpoint = readEndpointFile(endpointPath)
                        if taskEndpoint:
                            logger.debug('Task endpoint read: %s:%d' % taskEndpoint)
                            break
                        yield gen.sleep(self.check_time)
                        self.updateTaskState(task)
                    if task['taskEndpoint'].get('proxy', False) and taskEndpoint:
                        task['proxy'] = subprocess.Popen(
                            [sys.executable, '-m', 'everest_proxy.client',
                             '--endpoint-file=%s' % os.path.join(task['dir'], 'proxy.endpoint'),
                             self.config.get('proxyFrontendURI'), self.config.get('proxyBackendToken'),
                             taskEndpoint[0], str(taskEndpoint[1])])
                        proxyEndpoint = None
                        backendRunning = True
                        endpointPath = os.path.join(task['dir'], 'proxy.endpoint')
                        while task['state'] == 'RUNNING' and backendRunning:
                            backendRunning = task['proxy'].poll() is None
                            proxyEndpoint = readEndpointFile(endpointPath)
                            if proxyEndpoint:
                                break
                            yield gen.sleep(self.check_time)
                            self.updateTaskState(task)
                        if not backendRunning:
                            raise TaskException('Proxy backend terminated with code %d' %
                                                task['proxy'].returncode)
                        if proxyEndpoint:
                            task['taskEndpointOut'] = '%s:%d' % proxyEndpoint
                    elif taskEndpoint:
                        task['taskEndpointOut'] = '%s:%d' % taskEndpoint

                self.sendTaskState(task, 'RUNNING')
                while (task['state'] == 'RUNNING'):
                    yield gen.sleep(self.check_time)
                    self.updateTaskState(task)
                task['runTime'] = roundTime(time.time() - run_start)
            else:
                # we missed RUNNING state, so it lasted less than check period
                task['runTime'] = roundTime(self.check_time / 2)
                wait_time = roundTime((last_check_time - task['runTime']) - submit_start)
                if wait_time < 0:
                    wait_time = 0
                task['waitTime'] = wait_time
                # always send RUNNING state to pass wait time to server
                self.sendTaskState(task, 'RUNNING')

            if task['state'] == 'CANCELED':
                logger.info('CANCELED')
                task['reason'] = 'Task canceled'
                task['processTime'] = roundTime(time.time() - task['processTimeStart'])
                self.sendTaskState(task)
                return

            self.sendTaskState(task, 'COMPLETED')

            self.submitTask() # put new task's coroutine before httpclient's callback

            if task['outputData']:
                ret, err = yield self.doStageOut(self.pool, task['outputData'], task['dir'],
                                                 logger, task['dirName'])
                task.update(ret)
                if err and task['state'] == "COMPLETED":
                    task.update(err)
                self.sendTaskState(task, 'STAGED_OUT')

            if task['state'] == 'CANCELED':
                return
            if task['state'] == 'COMPLETED':
                task['state'] = 'DONE'
            assert(task['state'] in ['DONE', 'FAILED'])
            task['processTime'] = roundTime(time.time() - task['processTimeStart'])
            self.sendTaskState(task)
            logger.info('Done')
        except Exception as te:
            if not type(te) is TaskException:
                logger.exception("Task failed")
            logger.info('Task failed: %s' % te)
            task['state'] = 'FAILED'
            if task.get('reason', ''):
                task['reason'] += '; ' + str(te)
            else:
                task['reason'] = str(te)
            if not 'errorCode' in task:
                errorCode = getattr(te, 'error_code', None)
                task['errorCode'] = 6 if errorCode is None else errorCode
            task['processTime'] = roundTime(time.time() - task['processTimeStart'])
            self.sendTaskState(task)
            if 'pid' in task:
                self.TH.cancel(task['pid'])
        finally:
            if 'proxy' in task:
                try:
                    task['proxy'].terminate()
                except OSError:
                    pass
            self.submitTask()
            task['worker_done'].set_result(True)

    def sendTaskState(self, task, state = None):
        if state is None:
            state = task['state']
        data = {}
        for k, v in six.iteritems(task):
            if k in STATE_ATTRS[state]:
                if k == 'outputDataOut':
                    data['outputData'] = v
                elif k == 'taskEndpointOut':
                    data['taskEndpoint'] = v
                else:
                    data[k] = v
        self.sendTaskStateImpl(task, state, data)

    def sendTaskStateImpl(self, task, state, data = {}):
        self.sendMessageT(task['clientId'], 'TASK_STATE', task['id'], state, data)

    def sendMessageT(self, clientId, messageType, *args):
        if not clientId in self.connections:
            logger = logging.getLogger("everest_agent.sender")
            logger.warning("Can't send %s: no connection" %
                           messageType)
            return
        self.sendMessage(self.connections[clientId], messageType, *args)

    def sendMessage(self, conn, messageType, *args):
        message = json.dumps([messageType] + list(args))
        logger = logging.getLogger("everest_agent.sender")
        try:
            logger.debug("Sending message: %s" % message)
            conn['conn'].write_message(message)
        except Exception as e:
            logger.exception("Can't send a message, connection is lost. Trying to reconnect")
            logger.info(str(e))
            conn['conn'].close()
            self.cleanupConnection(conn['clientId'])

    def sendResourceInfo(self):
        try:
            resInfo = self.getResourceInfo()
            update = {'slots' : {}, 'tasks' : {}}
            nUpd = 0
            for k, v in six.iteritems(resInfo):
                if k == 'type':
                    continue
                for kk in v:
                    if resInfo[k][kk] != self.lastResourceInfo[k][kk]:
                        update[k][kk] = resInfo[k][kk]
                        nUpd += 1
            self.lastResourceInfo = resInfo
            if nUpd != 0:
                for clientId, conn in six.iteritems(self.connections):
                    self.sendMessage(conn, 'RESOURCE_INFO', update)
        finally:
            self.resinfo_handle = ioloop.IOLoop.current().call_later(
                self.config.get('resourceInfoPeriod'), self.sendResourceInfo)

    def submitTask(self):
        if len(self.task_queue) == 0:
            self.logger.debug("No tasks in queue")
            return

        if len(self.getAllTaskPids()) >= self.config.get('maxTasks'):
            self.logger.debug("No free slot for a new task")
            return

        self.logger.debug("Got a free slot. Resuming a task")
        future = self.task_queue.popleft()
        future.set_result(True)
