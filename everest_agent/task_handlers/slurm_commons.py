from __future__ import absolute_import
import subprocess
import re

from everest_agent.exceptions import TaskException

def get_job_state(jobId):
    cmd = "scontrol show job %s" % jobId
    try:
        p = subprocess.Popen(cmd.split(),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        (resp,err) = p.communicate()
        resp = resp.decode()
    except subprocess.CalledProcessError:
        raise TaskException("Failed to invoke scontrol command: %s" % cmd)

    if err:
        raise TaskException("Got error from scontrol command: %s" % err.decode())

    if not resp:
        return 'TIMEOUT'

    try:
        r = re.search("JobState=(\w+)",resp)
        s = r.group(1)
        if (s == 'RUNNING') or (s == 'COMPLETING'):
            state = "RUNNING"
        elif (s == 'PENDING') or (s == 'CONFIGURING') or (s == 'SUSPENDED'):
            state = "QUEUED"
        elif (s == 'COMPLETED'):
            state = "DONE"
        elif (s == 'CANCELLED'):
            state = "CANCELED"
        elif (s == 'FAILED'):
            r = re.search("ExitCode=(\d+):(\d+)",resp)
            exit_code = r.group(1)
            state = "FAILED ExitCode=" + exit_code
        else:
            state = "FAILED"
    except Exception:
        raise TaskException("Failed to parse scontrol response: %s" % resp)
    return state

def get_slots(queue):
    try:
        p = subprocess.Popen(["sinfo", '-p', queue, '-o','"%C"'  ],
                             stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        (resp,er) = p.communicate()
        #~ resp = subprocess.check_output(["qstat", "-f",str(pid)])
    except Exception as e:
        #~ raise e
        raise TaskException("Unable to get the status of the job")
    num_m = re.search("\d+/\d+/\d+/(?P<slots>\d+)", resp.decode())
    return int(num_m.group('slots'))

def get_free_slots(queue):
    try:
        p = subprocess.Popen(["sinfo", '-p', queue, '-o','"%C"'  ],
                             stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        (resp,er) = p.communicate()
        #~ resp = subprocess.check_output(["qstat", "-f",str(pid)])
    except Exception as e:
        #~ raise e
        raise TaskException("Unable to get the status of the job")
    num_m = re.search("\d+/(?P<slots>\d+)/\d+/\d+", resp.decode())
    return int(num_m.group('slots'))
