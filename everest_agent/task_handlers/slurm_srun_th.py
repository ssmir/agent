#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import os
import subprocess
import re
import json
import logging
import time
import traceback

import everest_agent.task_handlers.slurm_commons as slurm_commons
from everest_agent.exceptions import TaskException
import six

def create(config):
    return SlurmSrunTaskHandler(config)

class SlurmSrunTaskHandler:
    def __init__(self,config):
        self.logger = logging.getLogger("everest_agent.slurm_srun_th")
        self.logger.debug("Config: %r" % config)
        self.queue = config.get('queue', None)
        self.cpus = config.get('cpusPerTask', 1)
        self.timeLimit = config.get('timeLimit')
        self.tasksInJob = config.get('tasksPerJob', 8)
        self.collectTimeout = config.get('jobCollectTimeout', 4)
        self.checkTime = config.get('checkTime', 5)
        assert self.tasksInJob >= 1
        self.collecting = {}
        self.jobInfoCache = {}
        self.logger.debug("Active params: queue=%s, cpus=%d, tasksInJob=%d, collectTimeout=%d, checkTime=%d" % (
            self.queue, self.cpus, self.tasksInJob, self.collectTimeout, self.checkTime))

    # TODO: support resource requirements
    def submit(self, command, dir, env, requirements):
        if not os.access(dir, os.F_OK):  
            os.makedirs(dir)

        with open(os.path.join(os.path.abspath(dir),"jobfile"), "w") as f:
            f.write("#!/bin/bash\n")
            for k, v in six.iteritems(env):
                if v is None:
                    f.write('unset %s\n' % k)
                else:
                    f.write('export %s="%s"\n' % (k, v))
            f.write(command)

        groupName = os.path.basename(dir).split('-')[0]
        if not groupName in self.collecting:
            self.collecting[groupName] = {
                'tasks' : [],
                'submitTS' : time.time(),
                'state' : 0,
                'groupName' : groupName
            }
        job = self.collecting[groupName]
        if not requirements is None:
            timeLimit = requirements.get('timeLimit')
            if timeLimit is not None:
                if 'timeLimit' in job:
                    job['timeLimit'] = max(timeLimit, job['timeLimit'])
                else:
                    job['timeLimit'] = timeLimit
        if not 'timeLimit' in job and not self.timeLimit is None:
            job['timeLimit'] = self.timeLimit
        taskSeq = len(job['tasks'])
        job['tasks'].append(os.path.abspath(dir))
        self.logger.debug("Task added to job %s with sequence number %d" % (groupName, taskSeq))
        if len(job['tasks']) >= self.tasksInJob:
            try:
                self.submitSrunJob(job)
            except:
                job['state'] = 2
                raise
            finally:
                del self.collecting[groupName]
        return (job, taskSeq)

    def submitSrunJob(self, job):
        srunJobFile = os.path.join(job['tasks'][0], 'srjobfile')
        with open(srunJobFile, "w") as f:
            f.write("#!/bin/bash\n")
            f.write("#SBATCH -D %s\n" % job['tasks'][0])
            f.write("#SBATCH -e srstderr\n")
            f.write("#SBATCH -o srstdout\n")
            if self.queue:
                f.write("#SBATCH -p %s\n" % self.queue)
            f.write("#SBATCH -c %d\n" % self.cpus)
            f.write("#SBATCH -n %d\n" % len(job['tasks']))
            if 'timeLimit' in job:
                # convert seconds to "minutes:seconds"
                timeLimitStr = "%d:%d" % (job['timeLimit']/60, job['timeLimit']%60)
                f.write("#SBATCH --time=%s\n" % timeLimitStr)
            for i, taskDir in enumerate(job['tasks']):
                f.write("""( srun --exclusive -D %s --output %s --error %s -n1 -N1 bash %s; _ECODE=$?; echo errorcode %d $(ps -o etime= -p "$$") $_ECODE; exit $_ECODE ) &\n""" % (
                    taskDir, os.path.join(taskDir, 'stdout'),
                    os.path.join(taskDir, 'stderr'), os.path.join(taskDir, 'jobfile'), i))
                #f.write("PID%d=$!\n" % i)
            f.write("wait\n")
            # for i, taskDir in enumerate(job['tasks']):
            #     f.write("wait $PID%d\n" % i)
            #     f.write("echo errorcode %d $PID%d $?\n" % (i,i))
            #     f.write('echo Elapsed: $(ps -o etime= -p "$$")\n')
        try:
            os.chmod(srunJobFile, 0o777)
            p = subprocess.Popen(["sbatch", srunJobFile],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (out, err) = p.communicate()
            jobId = out.decode().split()
            if not jobId:
                raise TaskException("Unable to initiate SLURM, stderr: %s" % err.decode())
            jobId = int(jobId[-1])
            job['jobId'] = jobId
            job['state'] = 1
            self.logger.debug("Job %s successfully submitted with jobId %d" % (
                job['groupName'], jobId))
        except TaskException:
            raise
        except Exception:
            raise TaskException("Unable to initiate SLURM: %s" % traceback.format_exc())

    def getSlurmJobState(self, jobId):
        now = time.time()
        if jobId in self.jobInfoCache:
            state, ts = self.jobInfoCache[jobId]
            if now < ts + 15:
                return state

        state = slurm_commons.get_job_state(jobId)

        # TOFIX: little memory leak because we don't remove finished jobs from this dict
        # It's not easy to clean them so leave it as it is
        if state != 'TIMEOUT':
            self.jobInfoCache[jobId] = (state, now)

        return state

    def get_state(self, jobAndTaskSeq):
        (job, taskSeq) = jobAndTaskSeq
        if job['state'] == 0:
            if time.time() >= job['submitTS'] + self.collectTimeout:
                try:
                    self.submitSrunJob(job)
                except:
                    job['state'] = 2
                    raise
                finally:
                    del self.collecting[job['groupName']]
            return 'QUEUED'
        if job['state'] == 2:
            return 'FAILED'
        if job['state'] == 3:
            return 'CANCELED'
        assert job['state'] == 1

        ret = self.getSlurmJobState(str(job['jobId']))
        spl = ret.split(' ', 1)
        state = spl[0]

        if state in ['RUNNING', 'DONE', 'FAILED']:
            try:
                with open(os.path.join(job['tasks'][0], 'srstdout')) as f:
                    for l in f.readlines():
                        if 'errorcode' in l:
                            spl = l.split()
                            if int(spl[1]) == taskSeq:
                                code = spl[3]
                                if code != '0':
                                    return "FAILED ExitCode=" + code
                                else:
                                    return "DONE"
            except IOError:
                pass
        return ret

    def cancel(self, jobAndTaskSeq):
        (job, taskSeq) = jobAndTaskSeq
        if job['state'] in [2, 3]:
            return
        if job['state'] == 0:
            job['state'] = 3
            del self.collecting[job['groupName']]
            return
        if job['state'] == 2:
            return
        assert job['state'] == 1
        try:
            p = subprocess.call(["scancel", str(job['jobId'])])
        except subprocess.CalledProcessError:
            pass

    def get_slots(self):
        return slurm_commons.get_slots(self.queue)

    def get_free_slots(self):
        return slurm_commons.get_free_slots(self.queue)
    
    def get_resource_state(self):
        state = {}
        state['type'] = 'slurm'
        return json.dumps(state)
        
    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)
        
    def get_type(self):
        return 'slurm_srun'
