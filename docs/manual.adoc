= Everest Agent User Manual
{docdate}
:numbered:
:toc: right
:icons: font

== Introduction

== Installation

=== System Requirements

Everest agent is a cross-platform Python application supporting both Linux and Microsoft Windows.

The system should have Python 2 installed. Supported Python versions:

* Linux: Python 2.6 or 2.7,
* Windows: Python 2.7.

For running agent in active mode the host should have outbound connectivity with Everest server (HTTPS, port 443).

=== Installing Dependencies

The agent depends on the following Python libraries:

* http://www.tornadoweb.org/[Tornado] - version >= 4.3 and < 5.0. NOTE: Tornado 4.3 is the last version which runs on Python 2.6.

The recommended way to install these dependencies is via https://pip.pypa.io/[pip].

==== Installing Dependencies on Linux

. Installing pip (if you have pip installed, proceed to Step 2)
  * Download https://bootstrap.pypa.io/get-pip.py[get-pip.py]
    ** `wget https://bootstrap.pypa.io/get-pip.py`
  * If you have root access:
    ** Run `python get-pip.py` (may require sudo)
  * If you don't have root access: 
    ** Run `python get-pip.py --user`
    ** Add `~/.local/bin` directory to the PATH: `export PATH=$PATH:~/.local/bin`
  * Now you should be able to run `pip` from the command line

. Installing required Python packages
  * If you have root access:
    ** Run `pip install tornado==4.5.3` (may require sudo)
  * If you don't have root access:
    ** Run `pip install --user tornado==4.5.3`

==== Installing Dependencies on Windows

. Installing pip (if you have pip installed, proceed to Step 2)
  * Download https://bootstrap.pypa.io/get-pip.py[get-pip.py]
  * Run `python get-pip.py`
  * Add `C:\PythonXX\Scripts` to your PATH
  * Now you should be able to run `pip` from the command line

. Installing required Python packages
  * Run `pip install tornado==4.5.3`

=== Downloading Agent

The recommended way to download the agent by cloning its repository:

----
git clone https://gitlab.com/everest/agent.git
----

An alternative way is to download and extract one of these archives:

* https://gitlab.com/everest/agent/repository/archive.zip
* https://gitlab.com/everest/agent/repository/archive.tar.gz

NOTE: The directory with the agent will be referred below as `AGENT_HOME`.

=== Upgrading Agent

* If you cloned the agent repository:
 ** Run `git pull` inside `AGENT_HOME`.

* If you downloaded repository archive:
  ** Download the latest archive, extract it to a new directory.
  ** Copy `conf/agent.conf` file from the old agent directory to the new one.

== Configuration

The agent configuration is stored in a plain text file using a JSON format.

The configuration file can be placed in the following default paths checked by the agent upon startup:

* `/etc/everest_agent/agent.conf` (only for Linux)
* `USER_HOME/.everest_agent/agent.conf`
* `AGENT_HOME/conf/agent.conf`

If multiple configuration files are found, their settings are applied in the specified path order, possibly overriding each other.

It is also possible to specify a custom path to the configuration file via a command line option `-c (--config)` when starting the agent. In this case the configuration is read only from the specified file and the default paths are ignored.

=== Quick Start

. Copy default configuration template `AGENT_HOME/everest_agent/agent.conf.default` to `AGENT_HOME/conf/agent.conf`.

. Open `AGENT_HOME/conf/agent.conf` in a text editor.

. Revise and edit at least the following configuration parameters:

  * *resource.maxTasks*. This parameter sets maximum number of tasks processed simultaneously by the agent. Use it to limit the number of running tasks, e.g., to number of local CPU cores.

  * *protocol.activeMode.enabled*. If you don't want to use the agent with Everest server, set this parameter to `false`. Otherwise, copy and paste the corresponding resource token from Everest server to the *protocol.activeMode.agentToken* parameter.

  * *protocol.passiveMode.enabled*. If you don't want to use the passive mode, set this parameter to `false`. Otherwise, specify a list of allowed client tokens in the *protocol.passiveMode.clientTokens* parameter.

  * *security.allowedCommands*. Specify a list of commands allowed for execution by the agent. You can use regular expressions, e.g., `+++["/user/bin/solver1.*", "/usr/bin/solver2.*"]+++`. If you want to allow any commands, specify `[".*"]` (use this only in a trusted environment!).
  
. If you want to use non-default task handler (e.g., to run tasks on a cluster), check out <<Task Handler Configuration>>.

=== Configuration Parameters

==== resource

[cols="1,1,4"]
|===
|Parameter |Type |Description

|tasksDir
|string
|Path to directory for storing task data (absolute or relative to `AGENT_HOME`).

|maxTasks
|integer
|Maximum number of tasks processed simultaneously by the agent.

|checkTime
|number
|Interval in seconds between checking task state.

|utilityPoolSize
|integer or null
|Size of worker pool for time consuming tasks performed by agent. If set to null, the number of CPUs in the system is used.

|keepTaskDir
|boolean
|Do not delete task working directory inside `tasksDir` after the task is completed (useful for debugging).

|taskDirSize
|integer
|If keepTaskDir=True, specifies maximum `tasksDir` size in bytes.

|taskDirCleanupPeriod
|integer
|If keepTaskDir=True, specifies `tasksDir` cleanup period in seconds. During the cleanup, directories of tasks in DELETED state are removed until there are no such directories left or the size of `tasksDir` becomes less than `taskDirSize`.

|attributes
|object
|Arbitrary resource attributes as string key-value pairs.

|===

==== resource.taskHandler

[cols="1,1,4"]
|===
|Parameter |Type |Description

|type
|string
|Task handler type (local, docker, torque, slurm, sge, etc.)

|...
|...
|See handler specific parameters in <<Task Handler Configuration>>.

|===

==== protocol

[cols="1,1,4"]
|===
|Parameter |Type |Description

|resourceInfoPeriod
|integer
|Interval in seconds between sending _RESOURCE_INFO_ messages to clients.

|pingPeriod
|integer
|Interval in seconds between sending ping messages to clients.

|pongTimeout
|integer
|Timeout in seconds after receiving a pong message for closing client connection.

|===

==== protocol.activeMode

[cols="1,1,4"]
|===
|Parameter |Type |Description

|enabled
|boolean
|Flag that determines whether the active mode is enabled.

|serverURI
|URI
|The WebSocket server endpoint the agent should connect to.

|agentToken
|string
|Alphanumeric token used for authentication of the agent on the above server.

|===

==== protocol.passiveMode

[cols="1,1,4"]
|===
|Parameter |Type |Description

|enabled
|boolean
|Flag that determines whether the passive mode is enabled.

|agentPort
|integer
|Port number used for accepting incoming WebSocket client connections.

|clientTokens
|string[]
|Array of alphanumeric tokens used for authentication of the clients.

|uploadDir
|string
|Path to directory for storing uploaded files (absolute or relative to `AGENT_HOME`).

|===

==== protocol.http

[cols="1,1,4"]
|===
|Parameter |Type |Description

|maxUploadSize
|integer
|Maximum size of uploaded file in bytes.

|connectTimeout
|integer
|HTTP connect timeout in seconds for file uploads and downloads (see connect_timeout in tornado.httpclient.HTTPRequest).

|requestTimeout
|integer
|HTTP request timeout in seconds for file uploads and downloads (see request_timeout in tornado.httpclient.HTTPRequest).

|requestTries
|integer
|Number of tries for file uploads and downloads in case of Tornado's HTTP 599 timeout error. First attempt is made with connectTimeout and requestTimeout timeouts. Later attempts are made with timeouts random.uniform(t, t * 2.**(tries+1)) with tries from 1 to requestTries and t = connectTimeout or requestTimeout.

|cacheDir
|integer
|Path to directory for storing cache of downloaded files (absolute or relative to `AGENT_HOME`).

|maxCacheSize
|integer
|Maximum size of downloaded files cache in bytes.

|===

==== securtiy

[cols="1,1,4"]
|===
|Parameter |Type |Description

|allowedCommands
|string[]
|Array of commands allowed for execution. Regular expressions are suppported.

|===

==== logging

[cols="1,1,4"]
|===
|Parameter |Type |Description

|logDir
|string
|Path to directory for storing log files (absolute or relative to `AGENT_HOME`).

|level
|string
|Logging level.

|rotate
|boolean
|Flag that determines whether the log file rotation is enabled.

|maxSize
|integer
|Maximum size of log file to trigger rotation (ignored if rotation is disabed).

|backupCount
|integer
|Maximum number of stored log files (ignored if rotation is disabed).

|===

==== controlAPI

[cols="1,1,4"]
|===
|Parameter |Type |Description

|enabled
|boolean
|Flag that determines whether the Control API is enabled.

|port
|integer
|Port number used by Control API.

|===

==== taskProtocol

[cols="1,1,4"]
|===
|Parameter |Type |Description

|enabled
|boolean
|Flag that determines whether the task streaming protocol is enabled.

|address
|string
|IP address or hostname used for communication with running tasks. If agent is running on a cluster, this parameter has to be set to an address reacheable from other cluster nodes.

|port
|integer
|Port number used for communication with running tasks.

|===

=== Task Handler Configuration

==== local

This task handler runs tasks on a local machine as normal processes.

It doesn't have any parameters.

==== docker

This task handler runs tasks on a local machine inside disposable Docker containers.

[cols="1,1,4"]
|===
|Parameter |Type |Description

|image
|string
|Container image used for running tasks.

|mem_limit
|string
|Memory limit per task container (eg. 500m, 1g).

|network
|string
|Docker network name where container should be attached


|===

==== torque

This task handler runs tasks on a compute cluster with TORQUE manager.

[cols="1,1,4"]
|===
|Parameter |Type |Description

|queue
|string
|Cluster queue used for running tasks.

|===

==== slurm

This task handler runs tasks on a compute cluster with SLURM manager. Individual SLURM job is started for each Everest task.

[cols="1,1,4"]
|===
|Parameter |Type |Description

|queue
|string
|Cluster queue used for running tasks.

|cpusPerTask
|integer
|Number of CPUs to be requested from the job scheduler for each Everest task.

|===

==== slurm_srun

This task handler runs tasks on a compute cluster with SLURM manager. Submits SLURM jobs which contain multiple Everest tasks sharing the same Everest job id.

[cols="1,1,4"]
|===
|Parameter |Type |Description

|queue
|string
|Cluster queue used for running tasks.

|cpusPerTask
|integer
|Number of CPUs to be requested from the job scheduler for each Everest task.

|tasksPerJob
|integer
|Maximum number of Everest tasks to be aggregated in one SLURM job.

|jobCollectTimeout
|integer
|Time in seconds the agent waits before submitting a SLURM job with less than `tasksPerJob` tasks.

|timeLimit
|integer
|Time limit in seconds to be set for SLURM jobs. If set to null (default), partition's time limit will be used.

|===

==== sge

This task handler runs tasks on a compute cluster with SGE manager.

[cols="1,1,4"]
|===
|Parameter |Type |Description

|queue
|string
|Cluster queue used for running tasks.

|===

== Running Agent

To start the agent run the following command in `AGENT_HOME`:
----
python -m everest_agent.start
----

The following optional command-line parameters are supported:

[cols="1,4"]
|===
|Option |Description

|`-c`, `--config`
|Custom path to the agent configuration file. If specified then the default configuration paths are ignored.

|`-l`, `--log`
|Custom path to the agent log file. If specified then all logging configuration parameters (except `level`) are ignored.

|===

=== Linux Scripts

The following convenient scripts are provided in `AGENT_HOME/bin` to manage the agent on Linux:

* `start.sh` - run the agent in the background (supports the same command-line parameters),
* `stop.sh` - stop the agent,
* `restart.sh` - restart the agent.

=== Setting Up Automatic Startup on Linux

Agent's auto startup can be easily enabled on systems where Systemd is used as an init system.

The following script should be copied to `/etc/systemd/system/everest-agent.service`
replacing AGENT_HOME with actual agent's home directory,
AGENT_USER with the name of the user (not root) who owns AGENT_HOME and
AGENT_GROUP with the default group of AGENT_USER:
```
[Unit]
Description=Everest agent
After=network.target

[Service]
Type=forking
PIDFile=AGENT_HOME/agent.pid
WorkingDirectory=AGENT_HOME

User=AGENT_USER
Group=AGENT_GROUP

ExecStart=AGENT_HOME/bin/start.sh
ExecStop=AGENT_HOME/bin/stop.sh
ExecReload=AGENT_HOME/bin/restart.sh
TimeoutSec=30

[Install]
WantedBy=multi-user.target
```

Enable agent automatic startup:
```
sudo systemctl enable everest-agent.service
```

Start the newly created service:
```
sudo systemctl start everest-agent.service
```

== Using Graphical User Interface (experimental)

In order to use GUI install PySide or PyQt4.

To start GUI run the following command in `AGENT_HOME`:

----
python -m everest_agent.gui.main
----

In Linux you can also use a script `AGENT_HOME/bin/gui.sh`.

The following optional command-line parameters are supported:

[cols="1,4"]
|===
|Option |Description

|`-c`
|Custom path to the agent configuration file. Default path is `USER_HOME/.everest_agent/agent.conf`.

|`-d`
|Agent working directory. Default path is `USER_HOME/.everest_agent`.

|`-m`
|Run GUI minimized to tray.

|===

== Troubleshooting