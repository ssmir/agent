#!/bin/bash
BIN_DIR=`dirname "$(cd ${0%/*} && echo $PWD/${0##*/})"`
AGENT_HOME=`dirname "$BIN_DIR"`
cd $AGENT_HOME

PID_FILE=agent.pid

running() {
    ps -p $(cat agent.pid) -o pid | grep $(cat agent.pid) > /dev/null
}

if [ -f $PID_FILE ] && running; then
    PID=$(cat $PID_FILE)
    kill -SIGINT $PID
    while running; do
        sleep 1
    done
    rm $PID_FILE
    echo "Agent is stopped"
else
    echo "Agent is not running"
fi
