ARG PYTHON_VERSION=3.6-slim
FROM python:$PYTHON_VERSION as base

WORKDIR /agent

RUN apt-get update && apt-get install -y jq

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "bash", "bin/set_config_and_run.sh" ]


FROM base as test

WORKDIR /agent/test
RUN ./runtests.sh
